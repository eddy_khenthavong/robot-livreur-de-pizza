<?php

/**
 * La classe RobotLivreurPizza décrit le comportement du tout nouveau robot livreur de pizzas de JBHuet.com !
 *
 * Le nouveau robot livreur de pizzas de JBHuet.com est une formidable machine qui va chercher pour vous la pizza qui sort du four :
 * votre pizza préférée vient à vous, sans que vous ayez à bouger.
 * En pleine session de codage intense, pas possible de vous lever pour récupérer la pizza alors que le four sonne ? Pas de souci !
 * Le robot livreur de pizzas de JBHuet.com vous l'apporte sans effort, ou presque.
 * Cette merveilleuse machine se programme très simplement grâce à sa classe qui décrit très précisément tout ce qu'il peut faire pour vous.
 * Il vous suffit d'utiliser cette classe pour décrire à votre robot comment vous apporter votre délicieuse pizza.
 * 
 * Ce robot perfectionné est doté d'un écran pour afficher les informations dont vous pourriez avoir besoin.
 * Vous pouvez même lui faire vous souhaiter un bon appétit !
 * NB. : ce robot étant encore à l'état de prototype, l'écran fournit actuellement ne peut afficher que des messages de 255 caractères maximum.
 * Grâce à ses capteurs (testeur de pente dans le sens du déplacement, testeur de distance aux obstacles face à lui, testeur de position de la pizza par rapport au plateau)
 * le robot peut s'adapter à de nombreuses situations.
 *
 * @author Jean-Bernard HUET <contact@jbhuet.com>
 *
 * @version 1.0.0
 *
 * PS. : les robots livreurs de pizzas existent déjà https://www.youtube.com/watch?v=rb0nxQyv7RU et https://www.youtube.com/watch?v=mIwDhnPnb4o
 */
class RobotLivreurPizza
{

    //Attributs publics 
    //      (on va dire aucun pour le moment)

    // Attributs privés
    public $NomMachine;                // Le nom de la machine.
    public $NomMaitre;                 // Le nom du maître.

    /** @var string $Message Le message affiché à l'écran. */
    private $MessageEcran = "Bon appétit mon pote";

    /** @var integer CAPA_AFFICH Constante représentant le nombre maximal de caractères affichés à l'écran */
    private const CAPA_AFFICH = 255;

    /** @var integer Constante représtantant la hauteur min et max que le plateau de la pizza peut aller */


    /**
     * Titre de l'aide en ligne. Exemple : Création d'un objet de type RobotLivreurPizza
     *      Création d'un robot qui me livrera ma pizza de mon four de mon appartement à mon bureau dans ma chambre.
     * 
     * 
     * Description de l'aide en ligne. Exemple : À la création d'un objet de type RobotLivreurPizza...
     *
     * Renseigner la ligne suivante s'il y a un paramètre. Ajouter autant de ligne d'information que de paramètre de la fonction.
     * <Type du paramètre> <nom de la variable : $...> <Description du paramètre : à quoi sert-il, quelle(s) valeur(s) lui donner, etc. ?>
     * @param string
     * @param int
     * @param float
     */

    public function __construct(string $NomMc, string $NomMt)
    {
        // J'ai mis ça là, mais à vous de voir si c'est utile...
        $this->NomMachine = $NomMc;
        $this->NomMaitre = $NomMt;
    }

    // Méthodes publiques

    /**
     * Le robot parle avant de commencer sa première tâche.
     */

    public function Parler()
    {
        // Le robot affiche la phrase de bienvenue.
        printf(
            PHP_EOL . "Bonjour " . $this->NomMaitre . "! Je m'appelle " . $this->NomMachine . ". Je vais aller te chercher ta pizza. </br>"
        );
    }

    /**
     * Le robot avance et annonce la distance qu\'il va effectuer.
     */
    public function Avancer(float $Distance)
    {
        $this->AfficherMessage(sprintf(
            'J\'avance de %u mètres. </br>',
            $Distance
        ));
    }

    /**
     * Le robot recule et annonce la distance qu'il va effectuer.
     */
    public function Reculer(float $Distance)
    {
        $this->AfficherMessage(sprintf(
            'Je recule de %u mètres. </br>',
            $Distance
        ));
    }

    /**
     * Le robot tourne vers la droite et annonce le degré qu'il va effectuer.
     */
    public function TournerDroite(float $Degres)
    {
        $this->AfficherMessage(sprintf(
            'Je pivote de %u degrés vers la droite. </br>',
            $Degres
        ));
    }

    /**
     * Le robot tourne vers la gauche et annonce le degré qu'il va effectuer.
     */
    public function TournerGauche(float $Degres)
    {
        $this->AfficherMessage(sprintf(
            'Je pivote de %u degrés vers la gauche. </br>',
            $Degres
        ));
    }

    /**
     * Le robot doit monter ou descendre son plateau si il n'est pas aligné avec le four.
     */
    public function HauteurPlateau(int $Hauteur)
    {
        # Le robot possède un plateau pour transporter la pizza.
        # Au plus bas, le plateau est à 40cm au-dessus du sol.
        # Le plateau est fixé sur un verrin qui peut monter ou descendre.
        # Selon les modèles de robot, le verrin peut monter plus ou moins haut.
        # La hauteur maximale du verrin (donc du plateau) est fixe (une constante).
        # Je vous laisse libre de déteriner cette hauteur maximale.
        # Il faudra vérifier que le robot ne reçoit pas un ordre de monter le plateau en dehors de la plage (hauteur minimum / hauteur maximum) possible.
        # Le robot affiche la hauteur en centimètres à laquelle monte le plateau.

        if ($Hauteur <= 100 && $Hauteur >= 40) {
            if ($Hauteur <= 59 && $Hauteur >= 40) {
                $this->AfficherMessage(sprintf(
                    'Je monte le plateau pour atteindre une hauteur de %u. </br>',
                    $Hauteur
                ));
            }
            if ($Hauteur <= 100 && $Hauteur >= 61) {
                $this->AfficherMessage(sprintf(
                    'Je descends le plateau pour atteindre une hauteur de %u centimètres du sol. </br>',
                    $Hauteur
                ));
            } else {
                $this->AfficherMessage(sprintf(
                    'Le plateau est à la bonne hauteur. </br>',
                    $Hauteur
                ));
            }
        } else {
            $this->AfficherMessage(sprintf(
                'Veuillez renseigner une hauteur raisonnable. </br>',
                $Hauteur
            ));
        }
    }

    /**
     * Le robot va récuperer la pizza du four et la déposer sur le plateau.
     */
    public function TirerPizzaSurPlateau()
    {
        /*
         * À côté du plateau, une pince permet au robot d'attraper la pizza pour la glisser sur le plateau.
         * Le robot affiche un message de confirmation que la pizza a bien était tirée et est maintenant sur le plateau.
         */
        $this->AfficherMessage(sprintf('Je prends la pizza du four et la dépose sur le plateau. </br>'));
    }

    /**
     * Le robot va pousser la pizza du plateau et l'a déposer sur l'assiette sur mon bureau.
     */
    public function PousserPizzaSurPlateau()
    {
        /*
         * Au bord du plateau, le robot possède une spatule coudée qui se glisse sous la pizza pour la pousser hors du plateau.
         * Le robot affiche la confirmation qu'il a poussé la pizza hors du plateau.
         */
        $this->AfficherMessage(sprintf('Je push la pizza du plateau et la dépose sur l\'assiette de ton bureau. </br>Et voilà!</br> Ma mission est finie. </br>'));
    }

    /**
     * Le robot va pouvoir afficher ce qu'il fait sur son écran.
     */
    public function AfficherMessage(string $Message)
    {
        $this->MessageEcran = $this->TesterLongueurMessage($Message);
        print($this->MessageEcran);
    }

    public function BonAppetit()
    {
        /*
         * Au bord du plateau, le robot possède une spatule coudée qui se glisse sous la pizza pour la pousser hors du plateau.
         * Le robot affiche la confirmation qu'il a poussé la pizza hors du plateau.
         */
        $this->AfficherMessage(sprintf('Bon appétit mon pote! </br>'));
    }

    // Méthodes privées

    /**
     * Vérifie que le message à afficher ne dépasse pas la capacité de l'écran
     *
     * La méthode privée TesterLongueurMessage teste si le message passé en paramètre dépasse la capacité d'affichage de l'écran.
     * Si le message dépasse la capacité de l'écran, tous les caractères au-delà de la capacité maximale de l'écran sont supprimés
     *    et "..." est ajouté à la fin du nouveau message.
     * NB. : la longueur maximale du nouveau message ("..." compris) ne peut pas dépasser la capacité d'affichage de l'écran.
     *
     * @link https://www.php.net/manual/fr/language.oop5.constants.php Pour comprendre la notation self::CAPA_AFFICH
     * @link https://www.php.net/manual/fr/function.strlen.php Pour savoir ce que fait la fonction PHP strlen
     *
     * @param string $MessagePossible Valeur du message avant réduction de la longueur si celle-ci dépasse la capacité de l'écran
     * @return string Valeur du message après réduction éventuelle de la longueur
     */
    private function TesterLongueurMessage(string $MessagePossible): string
    {
        if (self::CAPA_AFFICH < strlen($MessagePossible)) {  // Vous pouvez modifier le code déja écrit...
            // Ecrire le code pour réduire la taille du message si nécessaire...
            substr($MessagePossible, 0, 252) . "...";
        }
        return $MessagePossible;
    }
}

// Ecrivez ci-dessous le code qui sera transmis à votre robot pour aller chercher votre pizza dans le four et l'apporter à votre bureau
// En imaginant que vous possédez ce robot, et que vous êtes installé·e à votre bureau, programmez le robot pour qu'il vous rapporte votre pizza toute chaude.
// Donnez des ordres au robot en fonction de la réalité de votre logement.
// Le robot peut partir de n'importe quel point (sous votre bureau, un placard, un coin de votre cuisine).
// On considère que la porte du four est ouverte, et qu'elle n'empêche pas le robot d'atteindre la pizza.
// Le robot doit déposer la pizza dans une assiette posée au bord de votre bureau.
// La dernière instruction que le robot devra exécuter est de vous souhaiter un bon appétit.

// programme principal

$Robot1 = new RobotLivreurPizza("C-18", "Eddy");
$Robot1->parler();
$Robot1->avancer(2);
$Robot1->TournerDroite(90);
$Robot1->HauteurPlateau(160); // test pour savoir si le robot comprend qu'il faut rentrer un entier compris entre 40 et 100.
$Robot1->HauteurPlateau(80);
$Robot1->HauteurPlateau(60);
$Robot1->TirerPizzaSurPlateau();
$Robot1->TournerGauche(90);
$Robot1->avancer(2);
$Robot1->TournerDroite(90);
$Robot1->avancer(8);
$Robot1->TournerDroite(90);
$Robot1->avancer(3);
$Robot1->TournerDroite(90);
$Robot1->PousserPizzaSurPlateau();
$Robot1->BonAppetit();

// FIN! Enjoy!